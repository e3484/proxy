package ru.tsystems.proxy.exceptions;

public class InstanceIsNotCreatedException extends RuntimeException {
    private final String message;

    public InstanceIsNotCreatedException(String message) {
        this.message = message;
    }
}
