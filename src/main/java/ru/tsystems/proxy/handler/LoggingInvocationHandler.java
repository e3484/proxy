package ru.tsystems.proxy.handler;

import ru.tsystems.proxy.service.ArgsService;
import ru.tsystems.proxy.service.LogAnnotationService;
import ru.tsystems.proxy.service.MethodParametersService;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class LoggingInvocationHandler<T> implements InvocationHandler {
    private final T myClass;

    public LoggingInvocationHandler(T myClass) {
        this.myClass = myClass;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (isInvokedMethodHasLogAnnotation(method)) {
            System.out.println("execute method: " + method.getName() + ", param: " + ArgsService.getArgs(args));
        }
        return method.invoke(myClass, args);
    }


    private boolean isInvokedMethodHasLogAnnotation(Method method) {
        var logMethods = LogAnnotationService.getMethodsWithLogAnnotation(method.getDeclaringClass());
        return logMethods.stream()
                         .anyMatch(logMethod -> logMethod.getName().equals(method.getName())
                                 && MethodParametersService.isEqualParametersInMethods(logMethod, method));
    }
}
