package ru.tsystems.proxy;


import ru.tsystems.proxy.logging.impl.TestLoggingImpl;

public class LoggingApplication {

    public static void main(String[] args) {
        var testLogging = new TestLoggingImpl();
//        var testLogging = ProxyService.createLogProxy(TestLogging.class);

        testLogging.calculation(6, 10);
        testLogging.calculation(13);
        testLogging.calculation();
        testLogging.anotherMethod();
        testLogging.calculation(2,3,"25");
    }
}
